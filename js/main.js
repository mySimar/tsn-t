$(document).ready(function() {
    //search
    $(".search-icon").on("click", openSearch);
    $("#search").on("blur", blurInput);
    //share
    $(".share-content").on("click", openShare);
    $(".close").on("click", closeModal);
    //video-post
    $(".f-s-video").on("click", openVideo);
    $(".close-video").on("click", closeVideo);
    //slider
    $(".slider-btn").on("click", slider);
    $(".slider-btn")[0].click();
});

$('body').click(function (event) {
    event.stopPropagation();
    event.preventDefault();
    //debugger
    if( !$(event.target).closest('.overlay').length && $('.modal-share').css('display') !== 'none') {
        $(".modal-share").hide();
    }
});
// search
function openSearch() {
    $("#search-form").toggleClass("search-focus");
    $("#search").focus();
}

function blurInput() {
    $("#search-form").removeClass("search-focus");
}
// share
function openShare(e) {
    e.preventDefault();
    e.stopPropagation();
    $(this).parents("body").find(".modal-share").show();
}

function closeModal(e) {
    e.preventDefault();
    e.stopPropagation();
    $(this).parents(".modal-share").hide();
}
// post video
function openVideo() {
    $(this).parents(".post-content-video").addClass("video-modal");
}

function closeVideo() {
    $(this).parents(".post-content-video").removeClass("video-modal");
}


$(window).scroll(function(){
    $el = $('.top-head');
    if ( $(this).scrollTop() > 150 ){
        $el.addClass('fixed');

        if( $(this).scrollTop() >= 400  ){
            $el.addClass('overlay-fixed');
            $(".top-head-left").hide();
            $(".top-menu").addClass('top-menu-fixed')
        }else{
            $el.removeClass('overlay-fixed');
            $(".top-head-left").show();
            $(".top-menu").removeClass('top-menu-fixed')
        }
    }else{
        $el.removeClass('fixed').removeClass('overlay-fixed'); //.removeClass('overlay-fixed');
    }
});

// slider

function slider() {
    // var sliderBlock = document.getElementsByClassName("top-news")[0];
    /*if (!sliderBlock.classList.contains("slider")) {
        sliderBlock.classList.add("slider");
        this.classList.add("act");
    } else {
        sliderBlock.classList.remove("slider");
        this.classList.remove("act");
    }*/
    var sliderArrow = document.querySelectorAll(".slider-btn-wrap .slider-btn");
    for (let i=0, max=sliderArrow.length; i<max; i++) {
        sliderArrow[i].classList.remove("act");
    };
    this.classList.toggle("act");
    var sliderContent = document.querySelectorAll("#top-news-slider li");
    for (let i=0, max=sliderContent.length; i<max; i++) {
        sliderContent[i].classList.remove("active-slide");
        sliderContent[i].style.display="none";
    };
    debugger
    sliderContent[$(this).index()].style.display="block";
    //this.sliderContent.classList.toggle("active-slide");
}